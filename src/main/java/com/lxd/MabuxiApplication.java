package com.lxd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MabuxiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MabuxiApplication.class, args);
    }

}
