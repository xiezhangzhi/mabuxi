package com.lxd;

import com.lxd.mapper.UserDaoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UserDaoMapper userDaoMapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
       //权限列表

        com.lxd.pojo.User
                myuser = userDaoMapper.find_user(s);
        List<GrantedAuthority> authorities= new ArrayList<GrantedAuthority>();

        authorities.add(new SimpleGrantedAuthority(s));
        String pwd = "";
        if(myuser!=null){
            pwd = myuser.getPwd();
        }
        //System.out.println(myuser.getPwd());
        User user  =
                new User(s,
                        pwd, true,
                        true,
                        true,
                        true, authorities);
        return user;
    }
}
