package com.lxd.ThreadText;

public class A extends Thread{
    static int ticket = 100;
   //通过构造方法给线程赋值
    public A(String name){
        super(name);
    }
//重写run方法 实现取票
    @Override
    public void run() {
       while (true){
           synchronized (this){
               if (ticket>0){

                   System.out.println(getName()+"卖出了第"+ticket+"张票");
                   ticket--;
               }else {
                   System.out.println("票卖完了");
                   break;
               }
           }
           try {
               sleep(100);
           }catch (Exception e){}
       }
    }

    public static void main(String[] args) {
        A a1 = new A("老王");
        A a2 = new A("老蒋");
        A a3 = new A("老张");
        a1.start();
        a2.start();
        a3.start();
    }
}
