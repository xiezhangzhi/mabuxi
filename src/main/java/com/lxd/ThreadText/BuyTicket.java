package com.lxd.ThreadText;

//public class BuyTicket implements Runnable {
public class BuyTicket extends Thread {
    private Ticket ticket;

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (this) {
                if (ticket.getTicketNum() > 0) {
                    ticket.sellTicket();
                    System.out.println(Thread.currentThread().getName() + "卖了一张票剩余" + ticket.getTicketNum() + "张票");
                } else {
                    System.out.println("卖完了");
                    break;
                }
            }
        }
    }

    public static void main(String[] args) {
        Ticket ticket = new Ticket();
        BuyTicket buyTicket = new BuyTicket();
        buyTicket.setTicket(ticket);
        Thread Thread1 = new Thread(buyTicket,"窗口1");
        Thread Thread2 = new Thread(buyTicket,"窗口2");
        Thread Thread3 = new Thread(buyTicket,"窗口3");
        Thread1.start();

        Thread3.start();
        Thread2.start();

    }

}