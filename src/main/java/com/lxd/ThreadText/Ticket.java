package com.lxd.ThreadText;

public class Ticket {
    private int ticketNum = 100000;
    public synchronized void sellTicket() {
        ticketNum --;
    }
    public synchronized int getTicketNum() {
        return ticketNum;
    }

}