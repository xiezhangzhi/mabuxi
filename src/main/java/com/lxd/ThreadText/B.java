package com.lxd.ThreadText;
public class B implements Runnable{
    static int ticket = 100;
    public void run() {
        while (true){
            synchronized (this){
                if (ticket>0){
                    System.out.println(Thread.currentThread().getName()+"卖出了第"+ticket+"张票");
                    ticket--;
                }else {
                    System.out.println("票卖完了");
                    break;
                }
            }
            try {
                Thread.sleep(100);
            }catch (Exception e){}
        }
    }

    public static void main(String[] args) {
        B b = new B();
       Thread b1 = new Thread(b,"老王");
        Thread b2 = new Thread(b,"老蒋");
        Thread b3 = new Thread(b,"老张");
        b1.start();
        b2.start();
        b3.start();
    }
}
