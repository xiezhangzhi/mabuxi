package com.lxd.pojo;


public class Provinces {

  private long id;
  private String provinceCode;
  private String province_name;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getProvinceCode() {
    return provinceCode;
  }

  public void setProvinceCode(String provinceCode) {
    this.provinceCode = provinceCode;
  }


  public String getProvinceName() {
    return province_name;
  }

  public void setProvinceName(String province_name) {
    this.province_name = province_name;
  }

}
