package com.lxd.pojo;


public class Address {

  private long addr_id;
  private long prov_id;
  private long city_id;
  private long distr_id;
  private String detailedAddress;
  private long defaultAddr;
  private long user_id;
  private Provinces provinces;
  private Cities cities;
  private Districts districts;
  private User user;


  public long getAddrId() {
    return addr_id;
  }

  public void setAddrId(long addr_id) {
    this.addr_id = addr_id;
  }


  public long getProvId() {
    return prov_id;
  }

  public void setProvId(long prov_id) {
    this.prov_id = prov_id;
  }


  public long getCityId() {
    return city_id;
  }

  public void setCityId(long city_id) {
    this.city_id = city_id;
  }


  public long getDistrId() {
    return distr_id;
  }

  public void setDistrId(long distr_id) {
    this.distr_id = distr_id;
  }


  public String getDetailedAddress() {
    return detailedAddress;
  }

  public void setDetailedAddress(String detailedAddress) {
    this.detailedAddress = detailedAddress;
  }


  public long getDefaultAddr() {
    return defaultAddr;
  }

  public void setDefaultAddr(long defaultAddr) {
    this.defaultAddr = defaultAddr;
  }


  public long getUserId() {
    return user_id;
  }

  public void setUserId(long user_id) {
    this.user_id = user_id;
  }

  public Provinces getProvinces() {
    return provinces;
  }

  public void setProvinces(Provinces provinces) {
    this.provinces = provinces;
  }

  public Cities getCities() {
    return cities;
  }

  public void setCities(Cities cities) {
    this.cities = cities;
  }

  public Districts getDistricts() {
    return districts;
  }

  public void setDistricts(Districts districts) {
    this.districts = districts;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
