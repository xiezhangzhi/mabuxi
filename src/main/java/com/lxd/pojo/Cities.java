package com.lxd.pojo;

public class Cities {

  private long id;
  private String cityCode;
  private String city_name;
  private long province_id;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getCityCode() {
    return cityCode;
  }

  public void setCityCode(String cityCode) {
    this.cityCode = cityCode;
  }


  public String getCityName() {
    return city_name;
  }

  public void setCityName(String city_name) {
    this.city_name = city_name;
  }


  public long getProvinceId() {
    return province_id;
  }

  public void setProvinceId(long province_id) {
    this.province_id = province_id;
  }

}
