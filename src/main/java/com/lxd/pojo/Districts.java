package com.lxd.pojo;


public class Districts {

  private long id;
  private String areaCode;
  private String area_name;
  private long city_id;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }
  public String getAreaCode() {
    return areaCode;
  }
  public void setAreaCode(String areaCode) {
    this.areaCode = areaCode;
  }
  public String getAreaName() {
    return area_name;
  }
  public void setAreaName(String area_name) {
    this.area_name = area_name;
  }
  public long getCityId() {
    return city_id;
  }
  public void setCityId(long city_id) {
    this.city_id = city_id;
  }

}
