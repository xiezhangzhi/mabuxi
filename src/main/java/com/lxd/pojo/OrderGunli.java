package com.lxd.pojo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.Date;
@Alias("orderGunli")
@Data
public class OrderGunli {
    private Integer id;
    private String order_number;
    private Date orderdate;
    private Integer user_id;
    private Integer address_id;
    private String wuliu;
    private String zhangtai;
}
