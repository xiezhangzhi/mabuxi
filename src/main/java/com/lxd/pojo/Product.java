package com.lxd.pojo;

import java.sql.Timestamp;

public class Product {

  private long id;
  private String pic;
  private double price;
  private long stock;
  private String title;
  private long buyCount;
  private java.sql.Timestamp createDate;
  private String detail;
  private double oldPrice;
  private String type1;
  private String type2;
  private long count;
  private long mer_id;
  private Merchant merchant;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getPic() {
    return pic;
  }

  public void setPic(String pic) {
    this.pic = pic;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public long getStock() {
    return stock;
  }

  public void setStock(long stock) {
    this.stock = stock;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public long getBuyCount() {
    return buyCount;
  }

  public void setBuyCount(long buyCount) {
    this.buyCount = buyCount;
  }

  public Timestamp getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }

  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public double getOldPrice() {
    return oldPrice;
  }

  public void setOldPrice(double oldPrice) {
    this.oldPrice = oldPrice;
  }

  public String getType1() {
    return type1;
  }

  public void setType1(String type1) {
    this.type1 = type1;
  }

  public String getType2() {
    return type2;
  }

  public void setType2(String type2) {
    this.type2 = type2;
  }

  public long getCount() {
    return count;
  }

  public void setCount(long count) {
    this.count = count;
  }

  public long getMer_id() {
    return mer_id;
  }

  public void setMer_id(long mer_id) {
    this.mer_id = mer_id;
  }

  public Merchant getMerchant() {
    return merchant;
  }

  public void setMerchant(Merchant merchant) {
    this.merchant = merchant;
  }
}
