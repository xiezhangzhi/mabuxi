package com.lxd.pojo;


import java.sql.Timestamp;

public class ShopCar {

  private long id;
  private long count;
 private User user;
 private Product product;
  private double totalPrice;
  private java.sql.Timestamp createDate;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public long getCount() {
    return count;
  }

  public void setCount(long count) {
    this.count = count;
  }
  public double getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(double totalPrice) {
    this.totalPrice = totalPrice;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Product getProduct() {
    return product;
  }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public void setProduct(Product product) {
    this.product = product;
  }
}
