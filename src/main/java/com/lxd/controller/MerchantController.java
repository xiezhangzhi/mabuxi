package com.lxd.controller;

import com.lxd.mapper.MerchantMapper;
import com.lxd.pojo.Merchant;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class MerchantController {
    @Resource
    private MerchantMapper merchantMapper;
    @Resource
    private RedisTemplate redisTemplate;
    @RequestMapping("getAllMerchant")
    public List<Merchant> getAllMerchant(){
        return merchantMapper.getAllMerchant();
    }
       @RequestMapping("merreg")
    public boolean RegMerchant(String name,String tel,String pwd,String yzm){
       boolean yzmIsRight= redisTemplate.opsForValue().get("tel").equals(yzm);
        if (!yzmIsRight){
            return false;
        }
        Merchant merchant = new Merchant(name,tel,pwd);
        return merchantMapper.addMerchant(merchant)==1;
    }
}
