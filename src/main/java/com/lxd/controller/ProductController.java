package com.lxd.controller;

import com.lxd.mapper.ProductMapper;

import com.lxd.pojo.Product;
import com.lxd.service.ProductService;
import com.lxd.util.FileUtil;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@CrossOrigin
public class ProductController {
    @Resource
    private ProductService productService;
    @Resource
    private ProductMapper productMapper;

    @PostMapping("getAllProduct")
    public Map<String, Object> getAllProduct(@RequestBody Map map) {
        Integer pageNum = (Integer) map.get("pageNum");
        Integer pageCount = (Integer) map.get("pageCount");
        String title = (String) map.get("title");
        String type1 = (String) map.get("type1");
        String type2 = (String) map.get("type2");
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>"+type1);
//        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>"+pageNum);


        return productService.getAllProduct(title,type1,type2,pageCount, pageNum);
    }

    @PostMapping("delProductById")
    public long delProductById(@RequestBody Map map) {
        Long id = Long.valueOf((Integer) map.get("id"));
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + id);
        return productMapper.delProductById(id);
    }
    @PostMapping("addProduct")
    public long addProduct(@RequestBody Map map){
        String title =(String) map.get("title");
        String type1 = (String) map.get("type1");
        String type2 = (String) map.get("type2");
        Double price =Double.valueOf((String) map.get("price"));
        String pic = (String) map.get("pic");
        int startIndex = pic.indexOf(">");
        int endIndex = pic.lastIndexOf("<");
        pic =pic.substring(startIndex + 1, endIndex);
        Long mer_id = Long.valueOf((String) map.get("mer_id"));
        return productMapper.addProduct(title,type1,type2,price,pic,mer_id);
    }

    @PostMapping("updateProduct")
    public long updateProduct(@RequestBody Product product){
        int startIndex = product.getPic().indexOf(">");
        int endIndex = product.getPic().lastIndexOf("<");
        String pic = product.getPic().substring(startIndex + 1, endIndex);
        product.setPic(pic);
        return productMapper.updateProduct(product);
    }

    @GetMapping(value = "/upload",produces = "text/javascript;charset=utf-8")
    public String configUpload(HttpServletRequest req, HttpServletResponse rep)throws IOException, ServletException {
        String action= req.getParameter("action");
        if (action!=null){
            String callback=req.getParameter("callback");
            String s = FileUtil.readFileAsString("c://ueditorconfig.txt");
            return callback+"("+s+")";
        }
        return null;
    }
    @PostMapping("upload")
    public Map handleFileUpload(String token,@RequestParam("file")MultipartFile file){
        String oldFileName = file.getOriginalFilename();
        int lastDotIndex = oldFileName.lastIndexOf(".");
        String extName = oldFileName.substring(lastDotIndex);
        String newName =  UUID.randomUUID()+extName;
        String os = System.getProperty("os.name");

        File excelFile = new File((os.startsWith("Windows")
                ?"c://":"/upload/") + newName);
        // System.out.println(excelFile.getAbsolutePath());
        try {
            file.transferTo(excelFile);
            Map map = new HashMap();
            map.put("original",oldFileName);
            map.put("size",file.getSize());
            map.put("state", "SUCCESS");
            map.put("title", newName);
            map.put("type", extName);
            map.put("url", "/find_img?fileName="+newName+"&token="+token);
            return map;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
