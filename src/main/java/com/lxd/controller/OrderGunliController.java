package com.lxd.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.lxd.pojo.OrderGunli;

import com.lxd.service.impl.OrderGunliServiceImpl;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@CrossOrigin
@RestController

public class OrderGunliController {
    @Resource
    private OrderGunliServiceImpl orderGunliService;
    @ResponseBody
    @PostMapping ("/findOrderGunli")
    public String findOrder(OrderGunli orderGunli){
        List<OrderGunli> list =orderGunliService.list();
        return JSON.toJSONStringWithDateFormat(list,"yyyy-MM-dd HH:mm:ss", SerializerFeature.DisableCircularReferenceDetect);
    }
}
