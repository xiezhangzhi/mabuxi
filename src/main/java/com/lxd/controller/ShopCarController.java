package com.lxd.controller;

import com.lxd.mapper.ShopCarMapper;
import com.lxd.pojo.ShopCar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class ShopCarController {
    @Autowired
    ShopCarMapper shopCarMapper;
    @PostMapping("getAllShopCar")
    public List<ShopCar> getAllShopCar(){
        return shopCarMapper.getAllShopCar();
    }
    @PostMapping("delShopCar")
    public long delShopCar(@RequestBody Map map){
        Long id = (Long)map.get("id");
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+id);
        return shopCarMapper.delShopCar(id);
    }
}
