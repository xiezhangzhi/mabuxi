package com.lxd.controller;

import com.lxd.mapper.AddressMapper;
import com.lxd.service.AddressService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@CrossOrigin
public class AddressController {
    @Resource
    private AddressService addressService;
    @Resource
    private AddressMapper addressMapper;
    @RequestMapping("getAllAddress")
    public Map<String,Object> getAllAddress(@RequestBody Map map){
        Integer pageCount = (Integer) map.get("pageCount");
        Integer pageNum = (Integer) map.get("pageNum");
        return addressService.getAllAddress(pageCount,pageNum);
    }
    @RequestMapping("delAddress")
    public Long delAddress(@RequestBody Map map){
        Long id = Long.valueOf((Integer)map.get("id"));
        return addressMapper.delAddress(id);
    }
}
