package com.lxd.controller;

import com.lxd.mapper.SystemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class SystemController {
    @Autowired
    SystemMapper systemMapper;
    @PostMapping("updatePassWord")
    public long updatePassWord(@RequestParam(required = false,defaultValue = "0",value = "id") long id,
                               @RequestParam(required = false,defaultValue = "0",value = "newPassword")String newPassword){
        return systemMapper.updatePassword( id,newPassword);
    }
}
