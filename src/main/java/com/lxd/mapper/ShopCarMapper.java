package com.lxd.mapper;

import com.lxd.pojo.ShopCar;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface ShopCarMapper {
    List<ShopCar> getAllShopCar();
    long delShopCar(@Param("id") long id);
}
