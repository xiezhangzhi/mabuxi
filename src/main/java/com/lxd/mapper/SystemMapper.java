package com.lxd.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface SystemMapper {
    long updatePassword(@Param("id") long id, @Param("newPassword") String newPassword);
}
