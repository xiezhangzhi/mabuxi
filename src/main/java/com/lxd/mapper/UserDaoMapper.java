package com.lxd.mapper;



import com.lxd.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;
@Mapper
public interface UserDaoMapper {
    int add_user(Map map);
    String is_user_openid_repeat(String openid);
    int update_user(Map map);
    int get_uid(Map map);

    User find_user(String nickName);
}
