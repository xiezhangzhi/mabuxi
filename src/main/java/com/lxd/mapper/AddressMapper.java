package com.lxd.mapper;

import com.lxd.pojo.Address;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AddressMapper {

    int getSumAddress();

    List<Address> getAllAddress(Integer rowNum, Integer pageCount);

    Long delAddress(@Param("id") Long id);
}
