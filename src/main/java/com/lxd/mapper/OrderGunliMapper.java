package com.lxd.mapper;

import com.lxd.pojo.OrderGunli;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface OrderGunliMapper {
    @Select("select * from order_gunli")
    List<OrderGunli> list();
}
