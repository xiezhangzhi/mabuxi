package com.lxd.mapper;

import com.lxd.pojo.Merchant;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MerchantMapper {
     List<Merchant> getAllMerchant();

     long addMerchant(Merchant shangJia);
}
