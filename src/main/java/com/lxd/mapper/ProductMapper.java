package com.lxd.mapper;

import com.lxd.pojo.Product;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ProductMapper {
    int getSumProduct(
            @Param("title")String title,
            @Param("type1")String type1,
            @Param("type2")String type2
    );

    List<Product> getAllProduct(
            @Param("title")String title,
            @Param("type1")String type1,
            @Param("type2")String type2,
            @Param("rowNum") Integer rowNum,
            @Param("pageCount") Integer pageCount);

    long delProductById(@Param("id")long id);

    long addProduct(@Param("title")String title,
                    @Param("type1")String type1,
                    @Param("type2")String type2,
                    @Param("price")Double price,
                    @Param("pic")String pic,
                    @Param("mer_id")Long mer_id);


    /**
     * 根据id修改商品信息
     * @param product
     * @return
     */
    Long updateProduct(Product product);
}
