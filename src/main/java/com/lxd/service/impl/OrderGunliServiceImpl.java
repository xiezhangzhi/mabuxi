package com.lxd.service.impl;

import com.lxd.mapper.OrderGunliMapper;
import com.lxd.pojo.OrderGunli;
import com.lxd.service.OrderGunliService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service("orderGunliServiceImpl")
public class OrderGunliServiceImpl implements OrderGunliService {
    @Resource
    private OrderGunliMapper orderGunliMapper;
    @Override
    public List<OrderGunli> list() {
        return orderGunliMapper.list();
    }
}
