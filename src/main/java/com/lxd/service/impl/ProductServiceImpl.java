package com.lxd.service.impl;

import com.lxd.mapper.MerchantMapper;
import com.lxd.mapper.ProductMapper;
import com.lxd.pojo.Merchant;
import com.lxd.pojo.Product;
import com.lxd.service.ProductService;
import com.lxd.util.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service("productService")
public class ProductServiceImpl implements ProductService {
    @Resource
   private ProductMapper productMapper;
    @Resource
    private MerchantMapper merchantMapper;
    @Override
    public Map<String,Object> getAllProduct(String title,String type1,String type2,Integer pageCount, Integer pageNum) {
        Map<String,Object> map = new HashMap<>();
        Page<Product> page = new Page<>();
        int sum = productMapper.getSumProduct(title,type1,type2);
        page.init(pageNum,pageCount,sum);
        List<Product> list = productMapper.getAllProduct(title,type1,type2,page.getRowNum(),pageCount);
        page.setList(list);
        map.put("page",page);
        return map;
    }

}
