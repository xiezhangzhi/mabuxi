package com.lxd.service.impl;

import com.lxd.mapper.AddressMapper;
import com.lxd.pojo.Address;
import com.lxd.service.AddressService;
import com.lxd.util.Page;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AddressServiceImpl implements AddressService {
    @Resource
    private AddressMapper addressMapper;
    @Override
    public Map<String, Object> getAllAddress(Integer pageCount, Integer pageNum) {
        Page<Address> page = new Page<>();
        int sum = addressMapper.getSumAddress();
        page.init(pageNum,pageCount,sum);
        List<Address> list = addressMapper.getAllAddress(page.getRowNum(),pageCount);
        Map<String,Object> map = new HashMap<>();
        page.setList(list);
        map.put("page",page);
        return map;
    }
}
