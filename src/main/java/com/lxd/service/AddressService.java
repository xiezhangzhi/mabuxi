package com.lxd.service;

import java.util.Map;

public interface AddressService {

    Map<String, Object> getAllAddress(Integer pageCount, Integer pageNum);
}
