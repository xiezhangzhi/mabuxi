package com.lxd.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpUtil {
    public static String getHTML(String urlstr){
        try {
            URL url = new URL(urlstr);
            HttpURLConnection con = (HttpURLConnection)
                    url.openConnection();

            BufferedReader br =
                    new BufferedReader(
                            new InputStreamReader(
                                    con.getInputStream(),"utf-8"
                            )
                    );

            StringBuffer sb = new StringBuffer();

            String s;

            while((s=br.readLine())!=null){
                sb.append(s+"\n");
            }

            return sb.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
