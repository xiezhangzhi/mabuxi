package com.lxd.util;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by Administrator on 2019/4/17.
 */
public class SignUtil {
    public static String sign(Map map){
        StringBuffer sb = new StringBuffer();
        String[] keyArr = (String[])
                map.keySet().toArray(new String[map.keySet().size()]);
        Arrays.sort(keyArr);
        for (int i = 0, size = keyArr.length; i < size; ++i) {
            sb.append(keyArr[i] + "=" + map.get(keyArr[i]) + "&");
        }
        sb.append("key=xxxx0881XXXX0881xxxx0881XXXX0881");
        String sign = string2MD5(sb.toString());
        return sign;
    }

    public static String string2MD5(String str){
        if (str == null || str.length() == 0) {
            return null;
        }
        char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f' };

        try {
            MessageDigest mdTemp = MessageDigest.getInstance("MD5");
            mdTemp.update(str.getBytes("UTF-8"));

            byte[] md = mdTemp.digest();
            int j = md.length;
            char buf[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                buf[k++] = hexDigits[byte0 >>> 4 & 0xf];
                buf[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(buf).toUpperCase();
        } catch (Exception e) {
            return null;
        }
    }
}
